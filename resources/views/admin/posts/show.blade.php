@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Post {{ $post->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/posts') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>
                        @can('edit_posts')
                        <a href="{{ url('/admin/posts/' . $post->id . '/edit') }}" title="Edit Post">
                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Edit
                            </button>
                        </a>
                        @endcan
                        @can('delete_posts')

                        <form method="POST" action="{{ url('admin/posts' . '/' . $post->id) }}" accept-charset="UTF-8"
                              style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Post"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                Delete
                            </button>
                        </form>
                        @endcan
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $post->id }}</td>
                                </tr>
                                <tr>
                                    <th> Title</th>
                                    <td> {{ $post->title }} </td>
                                </tr>
                                <tr>
                                    <th> Description</th>
                                    <td> {!! $post->description !!} </td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> {{ $post->status==1?'Active':'In-active' }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
